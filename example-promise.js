// function getTempCallback (location, callback) {
// 	callback(undefined, 78);
// 	callback("City not found");
// }

// getTempCallback("Lahore", function(err, temp){
// 	if(err){
// 		console.log('error', err);
// 	} else {
// 		console.log('success', temp);
// 	}
// });


// function getTempPromise(location) {
// 	return new Promise(function (resolve, reject) {
// 		setTimeout(function(){
// 			resolve(79);
// 			reject("City Nout Found");
// 		}, 1000);
// 	});
// }

// getTempPromise('Lahore').then(function(temp){
// 	console.log('promise success', temp);
// }, function(err) {
// 	console.log('promise error', err);
// });

// Challenge Area
function addPromise(a, b) {
	return new Promise(function(resolve, reject){
		if(typeof a === 'number' && typeof b === 'number'){
			resolve(a + b);
		} else {
			reject("A & B need to be number");
		}
	});
}

addPromise(12, 13).then(function(sum){
	console.log('success', sum);
}, function (err) {
	console.log('error', err);
});

addPromise('Faisal', 3).then(function(sum){
	console.log('success', sum);
}, function (err) {
	console.log('error', err);
});