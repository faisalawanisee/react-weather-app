var React = require('react');
var WeatherForm = require('WeatherForm');
var WeatherMessage = require('WeatherMessage');
var OpenWeatherMap = require('OpenWeatherMap');
var AlertMessage = require('AlertMessage');

var Weather = React.createClass({
	getInitialState: function(){
		return {
			isLoading: false,
			isError: false,
			ErrorMessage: ''
		}
	},
	handleSearch: function(location){
		var that = this;
		
		this.setState({isLoading: true, isError: false});

		OpenWeatherMap.getTemp(location).then(function(temp){
			that.setState({
				location: location,
				temp: temp,
				isLoading: false
			});
		}, function(errorMessage) {
			that.setState({
				isLoading: false,
				isError: true,
				ErrorMessage: "City Not Found"
			});
		});
		// this.setState({
		// 	location: location,
		// 	temp: 23
		// });
	},
	render: function(){
		var {isError, ErrorMessage, isLoading, temp, location} = this.state;

		function renderMessage() {
			if(isLoading) {
				return "Featching Weather..";
			} else if (temp && location){
				return <WeatherMessage temp={temp} location={location}/>
			}
		}

		function alertMessage() {
			if(isError){
				return <AlertMessage type="danger" message={ErrorMessage}/>
			}
		}

		return (
			<div className="d-flex justify-content-center text-center">
				<div className="col-5 margin-top-100">
					<div className="jumbotron">
						<WeatherForm onSearch={this.handleSearch}/>
						{renderMessage()}
						{alertMessage()}
					</div>
				</div>		
			</div>
		);
	}
});

module.exports = Weather;