var React = require('react');

var AlertMessage = React.createClass({
	render: function(){

		var {type, message} = this.props;

		return (
			<div className={ `alert alert-${this.props.type}` } role="alert">{message}</div>
		);
	}
});

module.exports = AlertMessage;