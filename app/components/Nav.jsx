var React = require('react');
var {Link, IndexLink} = require('react-router');

var Nav = React.createClass({
	render: function(){
		return (
			<nav className="navbar navbar-expand-sm bg-dark navbar-dark">
			  <a className="navbar-brand" href="#">Weather App</a>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			    <span className="navbar-toggler-icon"></span>
			  </button>
			  <div className="collapse navbar-collapse" id="collapsibleNavbar">
			    <ul className="nav nav-pills nav-fill">
			      <li className="nav-item">
			        <IndexLink to="/" activeClassName="active" className="nav-link">Get Weather</IndexLink>
			      </li>
			      <li className="nav-item">
			        <Link to="/about" activeClassName="active" className="nav-link">About</Link>
			      </li>
			      <li className="nav-item">
			        <Link to="/examples" activeClassName="active" className="nav-link">Examples</Link>
			      </li>    
			    </ul>
			  </div>  
			</nav>
		);
	}
});

module.exports = Nav;