var React = require('react');

var WeatherForm = React.createClass({
	onFormSubmit: function(e){
		e.preventDefault();
		var location = this.refs.location.value;

		if(location.length > 0){
			this.refs.location.value = "";
			this.props.onSearch(location);
		}
	},
	render: function(){
		return (
			<div className="margin-bottom-100">
				<h3 className="form-title">Weather component</h3>
				<form onSubmit={this.onFormSubmit}>
					<div className="form-group">
						<input type="text" className="form-control" id="city" placeholder="City" ref="location"/>
					</div>
					<button type="submit" className="btn btn-primary">Get Weather</button>
				</form>
			</div>
		);
	}
});

module.exports = WeatherForm;