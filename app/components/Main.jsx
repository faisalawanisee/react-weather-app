var React = require('react');
var Nav = require('Nav');

var Main = React.createClass({
	render: function(){
		return (
			<div id="main">
				<Nav />
				{this.props.children}
			</div>
		);
	}
});

module.exports = Main;